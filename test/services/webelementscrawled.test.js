const assert = require('assert');
const app = require('../../src/app');

describe('\'webelementscrawled\' service', () => {
  it('registered the service', () => {
    const service = app.service('webelementscrawled');

    assert.ok(service, 'Registered the service');
  });
});
