const assert = require('assert');
const app = require('../../src/app');

describe('\'sitestructures\' service', () => {
  it('registered the service', () => {
    const service = app.service('sitestructures');

    assert.ok(service, 'Registered the service');
  });
});
