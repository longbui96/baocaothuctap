const assert = require('assert');
const app = require('../../src/app');

describe('\'site_structures\' service', () => {
  it('registered the service', () => {
    const service = app.service('site-structures');

    assert.ok(service, 'Registered the service');
  });
});
