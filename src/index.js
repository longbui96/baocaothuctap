/* eslint-disable no-console */
// const winston = require('winston');
const app = require('./app');
const port = app.get('port');
const server = app.listen(port);

// const logger = winston.createLogger({
//   transports: [
//     new winston.transports.Console(),
//     new winston.transports.File({ filename: 'server.log' })
//   ]
// });

process.on('unhandledRejection', (reason, p) =>
  console.log('Unhandled Rejection at: Promise ', p, reason)
);

server.on('listening', () =>
  console.log('Feathers application started on http://%s:%d', app.get('host'), port)
);
