"use strict";

const crawlElement = require('./crawl-element');

module.exports = function () {
  // Add your custom middleware here. Remember, that
  // in Express the order matters
  const app = this; // eslint-disable-line no-unused-vars
  app.use('/crawl/element', crawlElement.crawlElement(app));
  app.use('/crawl/all-site', crawlElement.crawlAllSite(app));
};





