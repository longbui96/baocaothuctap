/* eslint-disable no-console,linebreak-style */
'use strict';
const crawler = require('../crawl-functions/crawler');

module.exports.crawlElement = function (app) {
  return function (req, res, next) {
    let recordedUser = 'auto';
    let id = req.query.id;
    let type = req.query.type;
    let recordId = parseInt(req.query.record_id);

    res.json({status: 'Crawling'});
    crawler.crawlElements(app, id, type, recordId, recordedUser).then(() => {
      console.log('[crawl-element-middleware][crawlElement] Crawling process done!');
    }).catch(error => {
      console.log('[crawl-element-middleware][crawlElement] Error crawling!', error);
    });
  };
};

module.exports.crawlAllSite = function (app) {
  return function (req, res, next) {
    let recordedUser = 'auto';
    let recordId = parseInt(req.query.record_id);

    res.json({status: 'Crawling'});
    crawler.crawlSites(app, recordId, recordedUser).then(() => {
      console.log('[crawl-element-middleware][crawlAllSite] Crawling process done!');
    }).catch(error => {
      console.log('[crawl-element-middleware][crawlAllSite] Error crawling!', error);
    });
  };
};

