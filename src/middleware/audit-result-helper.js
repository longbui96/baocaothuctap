"use strict";

module.exports = function (app) {
  return function (req, res, next) {
    const auditResultService = app.service('auditresults');
    const captureLogService = app.service('capturelogs');
    const auditRuleTemplateService = app.service('auditruletemplates');

    let capturedId = parseInt(req.query.capture_id);
    let templateId = req.query.template_id;

    let query = {};
    let checkLastestId = checkLastestCaptureID(captureLogService, capturedId);
    let checkTemplateIDs = checkTemplateID(auditRuleTemplateService, templateId);
    let waiter = [checkLastestId, checkTemplateIDs];

    Promise.all(waiter).then(results => {
      for (let key in results) {
        Object.assign(query, results[key]);
      }

      auditResultService.find({query : query}).then(results => {
        if (results.total === 0) {
          res.json(results);
        } else {
          let newResults = results.data;
          newResults.map(result => {
            for (let key in result) {
              if (result.hasOwnProperty(key) && result[key] instanceof Date) {
                result[key] = Math.floor(result[key].getTime() / 1000).toString();
              }
            }
          });
          results.data = newResults;
          res.json(results);
        }
      })
    }).catch(error => {
      res.json({"Error" : error});
    });
  };

  function checkLastestCaptureID(captureLogService, capturedIdToCheck) {
    return new Promise((resolve, reject) => {
      if (isNaN(capturedIdToCheck)) {
        getLastestCaptureId(captureLogService).then(foundCaptureId => {
          resolve({captureId : foundCaptureId});
        }).catch(error => {
          reject(error);
        });
      } else {
        resolve({captureId: capturedIdToCheck});
      }
    });
  }

  function checkTemplateID(auditRuleTemplateService, templateIdToCheck) {
    return new Promise((resolve, reject) => {
      if (!templateIdToCheck) {
        reject({'status' : 'Invalid templateId. It may has not been intilizated yet.'});
      } else {
        auditRuleTemplateService.find({query : { templateId: templateIdToCheck }}).then(result => {
          if (result.total > 0) {
            resolve({ ruleId: {$in : result.data[0].ruleIds}});
          } else {
            reject(`Template ID '${templateIdToCheck}' is not exist in DB.`);
          }
        }).catch(error => {
          console.log(`Error when get audit template by ID '${templateIdToCheck}'. Error:`, error);
          reject(error);
        });
      }
    });
  }

  function getLastestCaptureId(captureLogService) {
    return captureLogService.find({
      query: {
        "$sort": {captureId: -1},
        "$limit": 1
      }
    }).then(result => {
      if (result.total > 0) {
        return Promise.resolve(result.data[0].captureId);
      }
      return Promise.resolve(0);
    }).catch(error => {
      console.log(`Error when get lastest capture Id . Error \n: ${error}`);
      return Promise.reject(error);
    });
  }
};


