'use strict';

let comparatorObject = {
  weCrawledService: null,
  mongoClient: null,

  construct: (app) => {
    this.weCrawledService = app.service('webelementcrawleds');
    this.mongoClient = app.get('mongoClient');
  },

  compareElements: (currentCaptureId, comparedCaptureId) => {
    return new Promise ((resolve, reject) => {
      this.mongoClient.then(db => {
        db.collection('webelementcrawleds').aggregate([
          {
            $match: { captureId: { $in: [currentCaptureId, comparedCaptureId] } }
          },
          {
            $group: {
              _id: { weId: "$weId", index: "$index" },
              captureIds: { $push: {captureId : "$captureId", currentHtml : "$currentHtml"} }
            }
          },
          {
            $project: {
              _id: 0, weId : "$_id.weId", index : "$_id.index",
              capture: "$captureIds.captureId",
              html: "$captureIds.currentHtml"
            }
          },
          {
            $project: {
              weId: "$weId",
              index: "$index",
              currentId: { $arrayElemAt :  ["$capture", 1]},
              comparedId: { $arrayElemAt :  ["$capture", 0]},
              currentHtml: { $arrayElemAt :  ["$html", 1]},
              comparedHtml: { $arrayElemAt :  ["$html", 0]}
            }
          },
          {
            $project: {
              weId: "$weId",
              index: "$index",
              currentHtml: "$currentHtml",
              comparedHtml: "$comparedHtml",
              currentId: "$currentId",
              comparedId: "$comparedId",
              checkDifferent: { $eq : ["$currentHtml", "$comparedHtml"] }
            }
          },
          {
            $match: { "checkDifferent" : false }
          },
          {
            $out: "comparedelements_" + currentCaptureId + '_' + comparedCaptureId
          }
        ]).next((err, docs) => {
          if (err) {
            console.log(`Error when performing aggregate operation. Error message : ${err}`);
            reject(false);
          } else {
            resolve(true);
          }
        });
      }).catch(error => {
        console.log(`Error when try to connect to database. Error message : ${error}`);
        reject(false);
      })
    });
  },

  getComparingResult: (currentCaptureId, comparedCaptureId) => {
    return new Promise ((resolve, reject) => {
      this.mongoClient.then(db => {
        let collection_name = 'comparedelements_' + currentCaptureId + '_' + comparedCaptureId;
        db.collection(collection_name).find({}).toArray((err, docs) => {
          if (err) {
            console.log(`Error when trying to get comparing result. Error message : ${err}`);
            reject(err);
          } else {
            resolve(docs);
          }
        });
      }).catch(error => {
        console.log(`Error when try to connect to database. Error message : ${error}`);
        reject(err);
      });
    })
  }
};

module.exports = function(app) {
  return function(req, res, next) {
    comparatorObject.construct(app);
    let currentCaptureId = parseInt(req.query.current_capture_id);
    let comparedCaptureId = parseInt(req.query.compared_capture_id);
    if (isNaN(comparedCaptureId)) {
      comparedCaptureId = parseInt(currentCaptureId - 1);
    }
    comparatorObject.compareElements(currentCaptureId, comparedCaptureId).then(() => {
      comparatorObject.getComparingResult(currentCaptureId, comparedCaptureId).then(docs => {
        res.json({status : 'ok', data : docs});
      });
    }).catch(error => {
      console.log(`Error when comparing elements. Error message : ${error}`);
      res.json({status : 'failed', error : error});
    })
  };
};
