"use strict";

const helpers = require('../helpers.js');
module.exports.crawlElements = crawlElements;
module.exports.crawlSites = crawlSites;
const MOBILE_TYPE = 'mobile';

let crawlerObject = {
  weCrawledService: null,
  siteService: null,
  captureLogService: null,
  chromeDriver: null,
  webDriver: null,

  _construct: (app, siteType) => {
    crawlerObject.weCrawledService = app.service('webelementcrawleds');
    crawlerObject.captureLogService = app.service('capturelogs');
    crawlerObject.webDriver = app.get('webdriver');

    crawlerObject.siteService = app.service('sites');
    crawlerObject.chromeCapabilities = app.get('chromecapabilities');

    if (siteType === MOBILE_TYPE) {
      crawlerObject.chromeCapabilities = app.get('chromemobilecapabilities');
    }
    crawlerObject.chromeDriver = new crawlerObject.webDriver.Builder()
      .forBrowser('chrome')
      .withCapabilities(crawlerObject.chromeCapabilities)
      .build();
  },

  elementCrawler: (id, type, recordId, recordedUser) => {
    return new Promise((resolve, reject) => {
      let beginTime = 0;
      let endTime = 0;

      if (type !== 'section' && type !== 'web_element' && type !== 'page' && type !== 'site') {
        resolve({
          status: 'error',
          message: 'Invalid type of element. Type should be : <section> or <web_element> or <page> or <site>'
        });
      } else {
        let filter = {
          condition: {
            'pages.sections.elems.elemId': id
          },
          type: type,
          id: id
        };

        if (type === 'web_element') {
          beginTime = Date.now();
          crawlerObject.getElement(filter).then(result => {
            if (result === null) {
              resolve({
                status: 'error',
                message: `Web element ${id} not found`
              });
            } else {
              let pageUrl = result.pageUrl;
              let elem = result.data;

              let staticElements = [];
              let dynamicElements = [];

              if ('trigger' in elem && elem.trigger.length > 0) {
                dynamicElements.push(elem);
              } else {
                staticElements.push(elem);
              }

              console.log('-----------------------------------staticElements', staticElements);

              crawlerObject.crawlStaticElements(pageUrl, recordId, recordedUser, staticElements).then(() => {
                crawlerObject.crawlDynamicElement(pageUrl, recordId, recordedUser, dynamicElements, 0, crawlerObject.crawlDynamicElement).then(() => {
                  crawlerObject.chromeDriver.quit();
                  endTime = Date.now();
                  crawlerObject.saveNewRecordId(recordId, beginTime, endTime).then(() => {
                    console.log('===========DONE============');
                    resolve({
                      status: 'ok'
                    });
                  });
                });
              });
            }
          }).catch(error => {
            console.log(`Error when crawling dynamic elment ID ${id} with capture Id ${recordId}. Message : ${error}`);
            resolve({
              status: 'error',
              message: `Error when crawling element ${id}`
            });
          });
        }

        if (type === 'section') {
          filter = {
            condition: {
              'pages.sections.sectionId': id
            },
            type: type,
            id: id
          };
          crawlerObject.getElement(filter).then(result => {
            if (result === null) {
              resolve({
                status: 'error',
                message: `Section ${id} not found`
              });
            } else {
              beginTime = Date.now();
              let pageUrl = result.pageUrl;

              let staticElements = [];
              let dynamicElements = [];

              result.data.elems.map(elem => {
                if ('trigger' in elem && elem.trigger.length > 0) {
                  dynamicElements.push(elem);
                } else {
                  staticElements.push(elem);
                }
              });

              crawlerObject.crawlStaticElements(pageUrl, recordId, recordedUser, staticElements).then(() => {
                crawlerObject.crawlDynamicElement(pageUrl, recordId, recordedUser, dynamicElements, 0, crawlerObject.crawlDynamicElement).then(() => {
                  crawlerObject.chromeDriver.quit();
                  endTime = Date.now();
                  crawlerObject.saveNewRecordId(recordId, beginTime, endTime).then(() => {
                    console.log('===========DONE============');
                    resolve({
                      status: 'ok'
                    });
                  });
                });
              });
            }
          }).catch(error => {
            console.log(`Error when crawling dynamic elment ID ${id} with capture Id ${recordId}. Message : ${error}`);
            resolve({
              status: 'failed'
            });
          });
        }

        if (type === 'page') {
          beginTime = Date.now();
          crawlerObject.crawlPage(id, type, recordId, recordedUser).then(result => {
            if (result.status === 'error') {
              resolve(result);
            }
            crawlerObject.chromeDriver.quit();
            endTime = Date.now();
            crawlerObject.saveNewRecordId(recordId, beginTime, endTime).then(() => {
              console.log('===========DONE============');
              resolve({
                status: 'ok'
              });
            });
          });
        }

        if (type === 'site') {
          let filter = {
            condition: {
              'siteId': id
            },
            type: type,
            id: id
          };

          crawlerObject.getSiteElement(filter.condition).then(result => {
            beginTime = Date.now();
            if (result === null) {
              resolve({
                status: 'error',
                message: `Site ${id} not found`
              });
            } else {
              let pages = result.pages;
              crawlerObject.crawlSitePage(pages, 0, crawlerObject.crawlSitePage, recordId, recordedUser).then(result => {
                endTime = Date.now();
                crawlerObject.chromeDriver.quit();
                crawlerObject.saveNewRecordId(recordId, beginTime, endTime).then(() => {
                  console.log('===========DONE============');
                  resolve({
                    status: 'ok'
                  });
                });
              });
            }
          }).catch(error => {
            console.log(`Error when crawling site ID ${id} with capture Id ${recordId}. Message : ${error}`);
            resolve({
              status: 'failed'
            });
          });
        }
      }
    });
  },

  crawlPage: (id, type, recordId, recordedUser) => {
    return new Promise(resolve => {
      let filter = {
        condition: {
          'pages.pageId': id
        },
        type: type,
        id: id
      };

      crawlerObject.getElement(filter).then(result => {
        if (result === null) {
          resolve({
            status: 'error',
            message: `Page ${id} not found`
          });
        } else {
          let pageUrl = result.pageUrl;
          let staticElements = [];
          let dynamicElements = [];

          let sections = result.data.sections;

          sections.map(section => {
            section.elems.map(elem => {
              if ('trigger' in elem && elem.trigger.length > 0) {
                dynamicElements.push(elem);
              } else {
                staticElements.push(elem);
              }
            });
          });

          crawlerObject.crawlStaticElements(pageUrl, recordId, recordedUser, staticElements).then(() => {
            crawlerObject.crawlDynamicElement(pageUrl, recordId, recordedUser, dynamicElements, 0, crawlerObject.crawlDynamicElement).then(() => {
              resolve({
                status: 'done',
                message: `Page ${id} crawled!`
              });
            });
          });
        }
      }).catch(error => {
        console.log(`Error when crawling Page ID ${id} with capture Id ${recordId}. Message : ${error}`);
        resolve({
          status: 'error',
          message: `Page ${id} crawling get error ${error}`
        });
      });
    });
  },

  getStaticElement: (url, xPath, recordId) => {
    console.log('==================Begin crawling process for static contents=======================');
    console.log(`Begin to crawl static elements in Xpath ${xPath} at site ${url}`);
    crawlerObject.chromeDriver.get(url);
    let result = {
      status: false,
      total: 0,
      recordId: recordId,
      data: []
    };

    return crawlerObject.chromeDriver.findElements(crawlerObject.webDriver.By.xpath(xPath)).then(foundElements => {
      if (foundElements.length > 0) {
        console.log(`Found total ${foundElements.length} elements`);
        let getInnerHTMLPromises = [];
        foundElements.forEach((element, index) => {
          getInnerHTMLPromises.push(
            element.getAttribute('outerHTML').then(rawHTML => {
              return {index: index, value: crawlerObject.convertURIPath(rawHTML, url)};
            }).catch(() => {
              return {index: index, value: null};
            })
          );
        });
        return Promise.all(getInnerHTMLPromises).then(results => {
          results = results.filter(n => n);
          result.data = results;
          if (results.length > 0) {
            result.total = results.length;
            result.status = true;
            console.log(`Crawle elments at xpath ${xPath} of site ${url} succsefully!`);
          }
          return result;
        });
      } else {
        return result;
      }
    }).catch(error => {
      console.log(`Error when crawling elment at xpath ${xPath} of site ${url}. Message : ${error}`);
      return result;
    });
  },

  convertURIPath: (rawHtml, pageUrl) => {
    console.log("rawHTML:", rawHtml);
    console.log("pageUrl:", pageUrl);
    if (pageUrl instanceof String) {
      let siteURL = helpers.getAbsoluteURIPath(pageUrl);
    }
    // find all src and data-src

    // then replace them with path+ current URI path
    return rawHtml;
  },

  getDynamicElement: (url, triggers, resultXpath, recordId) => {

    return new Promise((resolve, reject) => {
      let result = {
        status: false,
        total: 0,
        recordId: recordId,
        data: []
      };
      console.log('==================Begin crawling process for dynamic contents=======================');
      // only process 1 element
      // TO DO: process for a chain of triggers
      if (triggers.length === 1) {
        let trigger = triggers[0];
        console.log(`Begin to crawl dynamic elements in Xpath ${trigger.xpath} at site ${url}`);
        let triggerXpath = trigger.xpath;
        let triggrerValue = trigger.value;
        let triggerAction = trigger.action;
        let waitXpath = trigger.waitXpath;
        let By = crawlerObject.webDriver.By;
        let until = crawlerObject.webDriver.until;

        crawlerObject.chromeDriver.get(url);

        if (triggerAction === 'sendKeys') {
          return crawlerObject.chromeDriver.findElement(By.xpath(triggerXpath)).click().then(() => {
            console.log('clicked!');
            return crawlerObject.chromeDriver.findElement(By.xpath(triggerXpath)).then(element => {
              element.sendKeys(triggrerValue);
              if (waitXpath === undefined || waitXpath === null) {
                waitXpath = resultXpath;
              }
              crawlerObject.chromeDriver.sleep(5000);
              return crawlerObject.chromeDriver.wait(until.elementLocated(By.xpath(waitXpath)), 10000).then(() => {
                console.log(`Element ${resultXpath} located!`);
                return crawlerObject.chromeDriver.findElements(By.xpath(resultXpath)).then(foundElements => {
                  if (foundElements.length > 0) {
                    // console.log(`Found total ${foundElements.length} elements`);
                    let getInnerHTMLPromises = [];
                    foundElements.forEach((element, index) => {
                      getInnerHTMLPromises.push(
                        element.getAttribute('outerHTML').then(rawHTML => {
                          return {index: index, value: rawHTML};
                        }).catch((error) => {
                          console.log(error);
                          return {index: index, value: null};
                        })
                      );
                    });
                    return Promise.all(getInnerHTMLPromises).then(results => {
                      results = results.filter(n => n);
                      result.data = results;
                      result.total = results.length;
                      result.status = true;
                      console.log(`Crawle elments at xpath ${resultXpath} of site ${url} succsefully!`);
                      resolve(result);
                    });
                  }
                  resolve(result);
                }).catch(error => {
                  console.log(`Error when crawling elment at xpath ${resultXpath} of site ${url}. Message : ${error}`);
                  resolve(result);
                });
              });
            });
          }).catch(error => {
            console.log(`Error when crawling dynamic elment at xpath ${triggerXpath} of site ${url}. Message : ${error}`);
            resolve(result);
          });
        }

        if (triggerAction === 'hover') {
          return crawlerObject.chromeDriver.findElement(By.xpath(triggerXpath)).then(element => {
            if (waitXpath === undefined || waitXpath === null) {
              waitXpath = resultXpath;
            }
            return crawlerObject.chromeDriver.actions().mouseMove(element).perform().then(() => {
              console.log('move mouse');
              return crawlerObject.chromeDriver.wait(until.elementLocated(By.xpath(waitXpath)), 10000).then(() => {
                console.log(`Element ${resultXpath} located!`);
                return crawlerObject.chromeDriver.findElements(By.xpath(resultXpath)).then(foundElements => {
                  if (foundElements.length > 0) {
                    let getInnerHTMLPromises = [];
                    foundElements.forEach((element, index) => {
                      getInnerHTMLPromises.push(
                        element.getAttribute('outerHTML').then(rawHTML => {
                          return {index: index, value: rawHTML};
                        }).catch((error) => {
                          console.log(error);
                          return {index: index, value: null};
                        })
                      );
                    });
                    return Promise.all(getInnerHTMLPromises).then(results => {
                      results = results.filter(n => n);
                      result.data = results;
                      result.total = results.length;
                      result.status = true;
                      console.log(`Crawle elments at xpath ${resultXpath} of site ${url} succsefully!`);
                      resolve(result);
                    });
                  }
                  resolve(result);
                });
              });
            });
          }).catch(error => {
            console.log(`Error when crawling dynamic elment at xpath ${triggerXpath} of site ${url}. Message : ${error}`);
            resolve(result);
          });
        }

      }
      resolve(result);
    });
  },

  saveElement: (weId, recordId, rawHtml, index, recordedUser, currentScreenShot) => {
    return new Promise((resolve, reject) => {
      crawlerObject.weCrawledService.find({
        query: {
          weId: weId,
          recordId: recordId,
          index: index
        }
      }).then(result => {
        if (result.total === 0) {
          crawlerObject.weCrawledService.create({
            weId: weId,
            currentHtml: rawHtml,
            recoredUser: recordedUser,
            recordId: recordId,
            index: index,
            currentScreenShot: currentScreenShot,
            createdAt: Date.now(),
            updatedAt: Date.now()
          }).then(() => {
            console.log(`Create element ${weId} of element Id ${weId} to database successfully!`);
            resolve({save: true});
          });
        } else {
          crawlerObject.weCrawledService.update(result.data[0]._id,
            {
              $set: {
                weId: weId,
                currentHtml: rawHtml,
                recoredUser: recordedUser,
                recordId: recordId,
                index: index,
                currentScreenShot: currentScreenShot,
                updatedAt: Date.now()
              }
            }).then(() => {
            console.log(`Update element ${weId} of element Id ${weId} to database successfully!`);
            resolve({save: true});
          });
        }
      }).catch(error => {
        console.log(`Error when save element ${weId} of element Id ${weId} to database. Error: \n ${error}`);
        resolve({save: false});
      });
    });
  },

  getElement: (filter) => {
    let elementId = filter.id;
    let keys = elementId.split('-');
    let pageId = keys[0] + '-' + keys[1];
    let sectionId = pageId + '-' + keys[2];

    return crawlerObject.getSiteElement(filter.condition).then(site => {
      if (site === null) {
        return null;
      }
      let page = site.pages ? site.pages.find(elem => {
        return elem.pageId === pageId;
      }) : null;
      if (page === null) {
        return null;
      }
      if (filter.type === 'page') {
        return {
          pageUrl: page.pageUrl,
          data: page,
          type: 'page'
        };
      }
      let sections = page.sections ? page.sections : null;
      let section = sections ? sections.find(elem => {
        return elem.sectionId === sectionId;
      }) : null;
      if (section === null) {
        return null;
      }

      if (filter.type === 'section') {
        return {
          pageUrl: page.pageUrl,
          data: section,
          type: 'section'
        };
      } else if (filter.type === 'web_element') {
        let webElement = section.elems ? section.elems.find(elem => {
          return elem.elemId === elementId;
        }) : null;
        if (webElement !== null) {
          return {
            pageUrl: page.pageUrl,
            data: webElement,
            type: 'web_element'
          };
        } else {
          return null;
        }
      } else {
        return null;
      }
    });
  },

  getSiteElement: (filter) => {
    return crawlerObject.siteService.find({
      query: filter
    }).then(result => {
      if (result.total > 0) {
        return result.data[0];
      }
      return null;
    }).catch(error => {
      console.log(`Error when get by filter ${filter}. Error \n: ${error}`);
      return null;
    });
  },

  getLastestCaptureId: () => {
    return crawlerObject.captureLogService.find({
      query: {
        '$sort': {captureId: -1},
        '$limit': 1
      }
    }).then(result => {
      if (result.total > 0) {
        return result.data[0].captureId;
      }
      return 0;
    }).catch(error => {
      console.log(`Error when get lastest capture Id . Error \n: ${error}`);
      return null;
    });
  },

  saveNewRecordId: (recordId, beginTime, endTime) => {
    return crawlerObject.captureLogService.create({
      captureId: recordId,
      beginTime: beginTime,
      endTime: endTime
    }).then(() => {
      console.log(`Save auto record Id ${recordId} to database successfully!`);
      return {save: true};
    }).catch(error => {
      console.log(`Error when save record Id ${recordId} to database. Error: \n ${error}`);
      return {save: false};
    });
  },

  crawlStaticElements: (pageUrl, recordId, recordedUser, staticElems) => {
    return new Promise((resolve, reject) => {
      let crawlPromises = [];
      if (staticElems && staticElems.length > 0) {
        staticElems.forEach(elem => {
          let getStaticContentPromise = crawlerObject.getStaticElement(pageUrl, elem.xpath, recordId).then(crawledObjects => {
            let staticWaiter = [];
            crawledObjects.data.forEach(rawHtmlObject => {
              // weId, recordId, rawHtmlObject, recoredUser, currentScreenShot
              staticWaiter.push(crawlerObject.saveElement(elem.elemId, recordId, rawHtmlObject.value, rawHtmlObject.index, recordedUser, null));
            });
            Promise.all(staticWaiter).then(results => {
              console.log(`Saving status of element Id ${elem.elemId}: ${results}`);
            }).catch(error => {
              console.log(`Error when saving crawled elements of element Id ${elem.elemId}: ${error}`);
            });
          });
          crawlPromises.push(getStaticContentPromise);
        });

        Promise.all(crawlPromises).then(() => {
          console.log('Crawl static element(s) done!');
          resolve({crawl: true});
        }).catch(error => {
          console.log('Crawl static element(s) has encoutered error! ', error);
          resolve({crawl: false});
        });
      } else {
        console.log('Static element array contain no element. Done!');
        resolve({crawl: false});
      }
    });
  },

  crawlDynamicElement: (pageUrl, recordId, recordedUser, elems, pos, callback) => {
    return new Promise((resolve, reject) => {
      let elem = elems[pos];
      if (pos < elems.length) {
        crawlerObject.getDynamicElement(pageUrl, elem.trigger, elem.xpath, recordId).then(crawledHtmlObjects => {
          let dynamicWaiter = [];
          crawledHtmlObjects.data.forEach(rawHtmlObject => {
            dynamicWaiter.push(crawlerObject.saveElement(elem.elemId, recordId, rawHtmlObject.value, rawHtmlObject.index, recordedUser, null));
          });
          Promise.all(dynamicWaiter).then(results => {
            console.log(`Saving status of element ID ${elem.elemId}: ${results}`);
            resolve(callback(pageUrl, recordId, recordedUser, elems, pos + 1, callback));
          })
        }).catch(error => {
          console.log(`Error when crawling dynamic element ID ${elem.elemId}: ${error}`);
          resolve(callback(pageUrl, recordId, recordedUser, elems, pos + 1, callback));
        });
      } else {
        console.log(`Done for ${elems.length} dynamic elements!`);
        resolve(true);
      }
    });
  },

  crawlSitePage: (pages, pos, callback, recordId, recordedUser) => {
    return new Promise((resolve, reject) => {
      let page = pages[pos];
      if (pos < pages.length) {
        console.log('------------------------------------------------------------', pos);
        crawlerObject.crawlPage(page.pageId, 'page', recordId, recordedUser).then((result) => {
          console.log(`Saving status of page ID ${page.pageId}: ${result}`);
          resolve(crawlerObject.crawlSitePage(pages, pos + 1, callback, recordId, recordedUser));
        }).catch(error => {
          console.log(`Error when crawling page ID ${page.pageId}: ${error}`);
          resolve(crawlerObject.crawlSitePage(pages, pos + 1, callback, recordId, recordedUser));
        });
      } else {
        console.log(`Done for ${pages.length} pages!`);
        resolve(true);
      }
    });
  },

  // test: () => {
  //   crawlerObject.chromeDriver.get('https://developers.google.com/web/updates/2017/04/headless-chrome');
  //   // crawlerObject.chromeDriver.findElement({name: 'q'}).sendKeys('webdriver');
  //   // crawlerObject.chromeDriver.findElement().click();
  //   // crawlerObject.chromeDriver.wait(crawlerObject.webDriver.until.titleIs('webdriver - Google Search'), 1000);
  //
  //   // Take screenshot of results page. Save to disk.
  //   crawlerObject.chromeDriver.takeScreenshot().then(base64png => {
  //     fs.writeFileSync('screenshot.png', new Buffer(base64png, 'base64'));
  //     console.log('DONE');
  //   });
  //   crawlerObject.chromeDriver.quit();
  //
  // },

  // takeScreenShoot: (elementSiteUrl, elementXpath, fileName) => {
  //    crawlerObject.webDriver.WebDriver.prototype.saveScreenshot = function(filename) {
  //      return crawlerObject.chromeDriver.takeScreenshot().then(function(data) {
  //          fs.writeFile(filename, data.replace(/^data:image\/png;base64,/,''), 'base64', function(err) {
  //              if(err) throw err;
  //          });
  //      })
  //   };
  //   return {saved: true};
  // },
};

function crawlElements(app, id, type, recordId, recordedUser) {
  let siteType = helpers.checkTypeOfElementId(id);
  crawlerObject._construct(app, siteType);
  if (isNaN(recordId)) {
    return crawlerObject.getLastestCaptureId().then(foundRecordId => {
      return crawlerObject.elementCrawler(id, type, foundRecordId + 1, recordedUser).then(() => {
        return Promise.resolve(true);
      }).catch(error => {
        console.log('[crawler] Error crawling!', error);
        return Promise.resolve(false);
      })
    });
  } else {
    return crawlerObject.elementCrawler(id, type, recordId, recordedUser).then(() => {
      return Promise.resolve(true);
    }).catch(error => {
      console.log('[crawler] Error crawling!', error);
      return Promise.resolve(false);
    })
  }
}

function crawlSites(app, recordId, recordedUser) {
  crawlerObject._construct(app);
  return crawlerObject.siteService.find({}).then(result => {
    if (result.total > 0) {
      let siteIds = result.data.map(site => {
        return site.siteId;
      });
      let index = 0;

      if (isNaN(recordId)) {
        return crawlerObject.getLastestCaptureId().then(foundRecordId => {
          return crawlSite(siteIds, index, app, foundRecordId + 1, recordedUser).then(() => {
            return Promise.resolve(true);
          }).catch(error => {
            console.log("[crawlAllSite] Error crawling site from list. Error: ", error);
            return Promise.reject(false);
          });
        }).catch(error => {
          console.log("[crawlAllSite] Error when get lastest record id. Error: ", error);
          return Promise.reject(false);
        });
      } else {
        return crawlSite(siteIds, index, app, recordId, recordedUser).then(() => {
          console.log('[crawlAllSite] All done!');
          return Promise.resolve(true);
        }).catch(error => {
          console.log("[crawlAllSite] Error crawling site from list. Error: ", error);
          return Promise.reject(false);
        });
      }
    } else {
      console.log("[crawlAllSite] List site is empty");
      return Promise.reject(false);
    }
  }).catch(error => {
    console.log('[crawlAllSite] crawlerObject.siteService.find() is error:', error);
    return Promise.reject(false);
  });
}

function crawlSite(siteIds, index, app, recordId, recordedUser) {
  return new Promise(function (resolve, reject) {
    if (index >= siteIds.length) {
      console.log('Done for all ' + siteIds.length + ' sites');
      resolve(true);
    }
    else {
      let siteId = siteIds[index];
      index++;

      return crawlElements(app, siteId, 'site', recordId, recordedUser).then(() => {
        console.log('[crawl-element-middleware] Crawling process done!');
        return crawlSite(siteIds, index, app, recordId, recordedUser);
      }).catch(error => {
        console.log('[crawl-element-middleware] Error crawling!', error);
        return crawlSite(siteIds, index, app, recordId, recordedUser);
      });
    }
  });
}
