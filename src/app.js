const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const bodyParser = require('body-parser');
global.rootPath = path.resolve(__dirname);

const feathers = require('feathers');
const configuration = require('feathers-configuration');
const hooks = require('feathers-hooks');
const rest = require('feathers-rest');
const socketio = require('feathers-socketio');

const handler = require('feathers-errors/handler');
const notFound = require('feathers-errors/not-found');

// var logger = require('feathers-logger');
// var winston = require('winston');

const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');

const mongodb = require('./mongodb');

const mongoose = require('./mongoose');
const webdriver = require('./webdriver');

const views = require('../views');

const app = feathers();

// Load app configuration
app.configure(configuration());
// Enable CORS, security, compression, favicon and body parsing

app.set('view engine', 'ejs');

app.use(cors());
app.use(helmet());
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// // Host the public folder
// app.use('/', feathers.static(app.get('public')));
// Views controller
// app.get('/',function (req, res) {
//   res.render('./containers/home-page/home-page.ejs');
// })
// Set up Plugins and providers
app.configure(hooks());
app.configure(mongoose);
app.configure(mongodb);
app.configure(webdriver);
app.configure(rest());
app.configure(socketio());

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
// Set up our services (see `services/index.js`)
app.configure(services);

// Set up our services (see `views/index.js`)
app.configure(views);
// configure feathers-logger using winston
// app.configure(logger(winston));
// Configure a middleware for 404s and the error handler

app.use(notFound());
app.use(handler());

app.hooks(appHooks);

module.exports = app;
