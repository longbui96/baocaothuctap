const webdriver = require('selenium-webdriver');
const chromeCapabilities = webdriver.Capabilities.chrome();
const chromeOptions = {
  args: ['--disable-gpu', '--headless', '--window-size=1024,768']
};
chromeCapabilities.set('chromeOptions', chromeOptions);

const chromeMobileCapabilities = webdriver.Capabilities.chrome();
const chromeMobileOptions = {
  args: ['--disable-gpu', '--headless'],
  mobileEmulation: {
    deviceName: 'iPhone 6'
  }
};
chromeMobileCapabilities.set('chromeOptions', chromeMobileOptions);

module.exports = function () {
  const app = this;
  app.set('webdriver', webdriver);
  app.set('chromecapabilities', chromeCapabilities);
  app.set('chromemobilecapabilities', chromeMobileCapabilities);
};

