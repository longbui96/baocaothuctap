// site-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const {Schema} = mongooseClient;

  const element = new Schema({
    elemId: {type: String, required: true},
    xpath: {type: String},
    trigger: {type: Array}
  }, {_id: false});

  const section = new Schema({
    sectionId: {type: String, required: true},
    sectionName: {type: String},
    xpath: {type: String},
    elems: [element]
  }, {_id: false});

  const page = new Schema({
    pageId: {type: String, required: true},
    pageUrl: {type: String, required: true},
    sections: [section]
  }, {_id: false});

  const site = new Schema({
    siteId: {type: String, required: true},
    siteUrl: {type: String, required: true},
    pages: [page],
    createdAt: {type: Date, default: Date.now},
    updatedAt: {type: Date, default: Date.now}
  });

  return mongooseClient.model('site', site);
};
