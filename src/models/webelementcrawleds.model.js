// webelementcrawleds-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const webelementcrawleds = new Schema({
    recordId: { type: Number, required: true },
    weId: { type: String, required: true },
    currentHtml: { type: String },
    recoredUser: { type: String },
    index: { type: Number },
    currentScreenShot: { type: String },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
  });

  return mongooseClient.model('webelementcrawleds', webelementcrawleds);
};
