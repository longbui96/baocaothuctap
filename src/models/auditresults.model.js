// auditresults-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const auditresults = new Schema({
    ruleId: { type: String, required: true },
    captureId: { type: Number, required: true },
    auditValue: { type: Number, required: true },
    auditTime: { type: Date },
    auditComment: { type: String },
  }, {
    timestamps: true
  });

  return mongooseClient.model('auditresults', auditresults);
};
