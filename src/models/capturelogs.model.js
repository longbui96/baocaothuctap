// capturelogs-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const capturelogs = new Schema({
    captureId: { type: Number, required: true, unique : true},
    beginTime: { type: Date, default: Date.now },
    endTime: { type: Date, default: Date.now }
  });

  return mongooseClient.model('capturelogs', capturelogs);
};
