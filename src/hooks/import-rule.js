"use strict";
module.exports.generateRuleFunction = generateRuleFunction;
module.exports.createNewFile        = createNewFile;

const fs = require('fs');

const RULE_CONTENT = 'ruleFnContent';
const RULE_NAME = 'ruleFnName';
const USER_FN_NAME = 'rule-functions-user';
const USER_FN_PATH = rootPath + `/${USER_FN_NAME}/`;

function generateRuleFunction(hook) {
  let savedData = hook.result;
  let auditFnContent = savedData[RULE_CONTENT];
  let auditFnName = savedData[RULE_NAME] ;
  let path = USER_FN_PATH;

  if (validateAuditRuleFnContent(auditFnContent) === false) {
    console.log(`Content of audit rule function is invalid, cannot create file name ${auditFnName} at ${path}`);
  } else {
    createNewFile(path, auditFnName + ".js", auditFnContent).then(() => {
      console.log(`Create new audit file name ${auditFnName} at ${path} successfuly`);
      let content = `\nmodule.exports.${auditFnName} = require(\'./${auditFnName}\').${auditFnName};`;
      updateRuleFunctionList(content).then(() => {
        console.log(`Updated file index.js: Added function name ${auditFnName}`);
      })
    }).catch(() => {
      console.log(`Cannot create new audit file name ${auditFnName} at ${path}`);
    })
  }
}

function validateAuditRuleFnContent(content) {
  // ODO, validate JS content here.
  return false;
}

function createNewFile(path, fileName, fileContent) {
  return new Promise((resolve, reject) => {
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }
    fs.writeFile(path + fileName, fileContent, err => {
      if(err) {
        console.log(`Cannot create new file ${fileName} at ${path}. Error:`, err);
        reject(false);
      } else {
        resolve(true);
      }
    })
  });
}

function updateRuleFunctionList(content) {
  return new Promise((resolve, reject) => {
    checkExistsFunction(content).then(result => {
      if (!result) {
        let indexPath = USER_FN_PATH + 'index.js';
        fs.appendFile(indexPath, content, function (err) {
          if(err) {
            console.log(`Cannot update file at ${indexPath}. Error:`, err);
            reject(false);
          } else {
            resolve(true);
          }
        });
      } else {
        resolve(true);
      }
    });
  });
}

function checkExistsFunction(contentToCheck) {
  return new Promise((resolve, reject) => {
    fs.readFile(USER_FN_PATH + 'index.js', function read(err, data) {
      if (err) {
        throw err;
      }
      if (data.indexOf(contentToCheck) > -1) {
        resolve(true);
      }
      resolve(false);
    });
  });
}


