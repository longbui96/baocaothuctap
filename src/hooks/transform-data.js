"use strict";
module.exports.convertToUnixTimeAfterFindHook = convertToUnixTimeAfterFindHook;

function convertToUnixTimeAfterFindHook(hook) {
  let results = hook.result;
  if (results.total === 0) {
    return Promise.resolve(hook);
  } else {
    results.data.map(result => {
      for (let key in result) {
        if (result.hasOwnProperty(key) && result[key] instanceof Date) {
          result[key] = Math.floor(result[key].getTime() / 1000).toString();
        }
      }
    });
    hook.result = results;
    return Promise.resolve(hook);
  }
}
