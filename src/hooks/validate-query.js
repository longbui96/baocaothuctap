"use strict";
const errors = require('feathers-errors');

module.exports = function () {
  return function (hook) {
    const inputQuery = hook.params.query;
    console.log("input query:", inputQuery);
    let isEmpty = !Object.keys(inputQuery).length;
    if (isEmpty) {
      throw new errors.BadRequest({'status' : 'Error!', 'message' : 'You should add query to your PUT/PATCH/DELETE requests'});
    } else {
      return Promise.resolve(hook);
    }
  };
};
