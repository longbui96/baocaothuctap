"use strict";
const errors = require('feathers-errors');
const helpers = require('../helpers.js');

module.exports = function () {
  return function (hook) {
    const inputData = hook.data;
    let keys = helpers.detectKeyByServiceName(hook.path);

    let query = {};
    for (let index in keys) {
      let fieldName = keys[index];
      query[fieldName] = inputData[fieldName];
    }

    return hook.service.find({query: query}).then(result => {
      if (result.total !== 0) {
        return hook.service.patch(result.data[0]._id, {$set : inputData}, {query : query}).then(status => {
          hook.result = status;
          return Promise.resolve(hook);
        });
      } else {
        return Promise.resolve(hook);
      }
    }).catch(error => {
      throw new errors.BadRequest({errors: error});
    });
  };
};
