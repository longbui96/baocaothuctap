"use strict";

let mappingFunctions = [];
module.exports.create = function () {
  for (let i = 0; i < arguments.length; i++) {
    mappingFunctions.push(arguments[`${i}`]);
  }
};
module.exports.getFunctionByName = function (functionName) {
  let resultFunction = null;
  mappingFunctions.map(object => {
    if (functionName in object) {
      resultFunction = object[functionName];
    }
  });
  return resultFunction;
};

module.exports.detectKeyByServiceName = function (serviceName) {
  switch (serviceName) {
    case 'auditresults':
      return ['ruleId', 'captureId'];
      break;
    case 'auditrules':
      return ['ruleId'];
      break;
    case 'auditruletemplates':
      return ['templateId'];
      break;
    case 'sites':
      return ['siteId', 'captureId'];
      break;
    case 'structureddatas':
      return ['mapFnId', 'elemId', 'captureId', 'index'];
      break;
    case 'mapfunctions':
      return ['mapFunctionId', 'weId'];
      break;
    case 'webelementcrawleds':
      return ['weId', 'recordId', 'index'];
      break;
  }
};
module.exports.getLastestCaptureId = function (captureLogService) {
  if (!captureLogService) {
    return 'Undefined input captureLogService';
  } else {
    return captureLogService.find({
      query: {
        "$sort": {captureId: -1},
        "$limit": 1
      }
    }).then(result => {
      if (result.total > 0) {
        return result.data[0].captureId;
      }
      return 0;
    }).catch(error => {
      console.log(`Error when get lastest capture Id . Error \n: ${error}`);
      return null;
    });
  }
};
module.exports.createResult = function (type, resultObject, message) {
  resultObject.status = type;
  resultObject.message = message;
};

module.exports.getAbsoluteURIPath = function (siteIdString) {
  let matches = siteIdString.match(/^https?:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
  return matches && matches[1];
};

module.exports.checkTypeOfElementId = (elementId) => {
  if (elementId) {
    let dotIndex = elementId.indexOf('.');
    if (dotIndex > -1) {
      if (elementId[dotIndex - 1] === 'm') {
        return 'mobile';
      }
      if (elementId[dotIndex - 1] === 'a') {
        return 'app';
      }
    }
  }
  return null;
};

module.exports.convertWeIdToKeywordId = (elementId, keyword) => {
  let keys = elementId.split('-');
  let pageId = keys[0] + '-' + keys[1];
  if (keyword === 'page') {
    return pageId;
  }
  if (keyword === 'section') {
    return pageId + '-' + keys[2];
  }
};

module.exports.testCollection = (database, collection, callback) => {
  console.log('************************BEGIN TEST****************************');
  console.log('1. Connected DB: \n', database);
  if (!database) {
    throw Error('Invalid database connection');
  }
  return database.collection(collection).findOne({})
    .then(result => {
      console.log(`2. Test result of collection ${collection}:\n`, result);
      console.log('************************END TEST******************************');
      if (callback) {
        callback(null, result);
      } else {
        return Promise.resolve(result);
      }
    })
    .catch(error => {
      console.log('************************END TEST******************************');
      if (callback) {
        callback(error);
      } else {
        return Promise.resolve(result);
      }
    });
};

module.exports.getListSiteId = (siteIdService, siteIds) => {
  if (!siteIdService) {
    return Promise.reject('Invalid `sites` service. Please check again!');
  }
  console.log("--------------", siteIds);

  let query = {};
  if (siteIds && siteIds.length > 0) {
    query.siteId = {$in: siteIds}
  }

  return siteIdService.find(query)
    .then(res => {
      if (res.total === 0) {
        return Promise.reject('Dont have any site Id in DB!');
      }
      let result = res.data.map(site => {return site.siteId});
      return Promise.resolve(result);
    })
    .catch(err => {
      return Promise.reject(err);
    })
};
