"use strict";
const errors = require('feathers-errors');
const convertToUnixTime = require('../../hooks/transform-data').convertToUnixTimeAfterFindHook;


function getLastestCaptureId(hook) {
  return new Promise((resolve) => {
    if ('lastest' in hook.params.query && hook.params.query.lastest === 'true') {
      hook.service.find({
        query: {
          "$sort": {captureId: -1},
          "$limit": 1
        }
      }).then(result => {
        hook.result = result;
        resolve(hook);
      }).catch(error => {
        console.log(`Error when get lastest capture Id . Error \n: ${error}`);
        throw new errors.BadRequest({errors: error});
      });
    } else {
      resolve(hook);
    }
  })
}


module.exports = {
  before: {
    all: [],
    find: [getLastestCaptureId],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [convertToUnixTime],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
