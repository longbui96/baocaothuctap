const checkDuplicateObject = require('../../hooks/validate-duplicate');
const checkValidQuery = require('../../hooks/validate-query');
const convertToUnixTime = require('../../hooks/transform-data').convertToUnixTimeAfterFindHook;

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkDuplicateObject()],
    update: [checkValidQuery()],
    patch: [checkValidQuery()],
    remove: [checkValidQuery()]
  },

  after: {
    all: [],
    find: [convertToUnixTime],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
