// Initializes the `sites` service on path `/sites`
const createService = require('feathers-mongoose');
const createModel = require('../../models/sites.model');
const hooks = require('./sites.hooks');
const filters = require('./sites.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'sites',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/sites', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('sites');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
