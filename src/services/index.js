const webelementcrawleds = require('./webelementcrawleds/webelementcrawleds.service.js');
const sites = require('./sites/sites.service.js');
const capturelogs = require('./capturelogs/capturelogs.service.js');
const auditresults = require('./auditresults/auditresults.service.js');

module.exports = function () {
  const app = this; // eslint-disable-line no-unused-vars
  app.configure(webelementcrawleds);
  app.configure(sites);
  app.configure(capturelogs);
  app.configure(auditresults);
};
