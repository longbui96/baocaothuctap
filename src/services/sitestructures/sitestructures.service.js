// Initializes the `sitestructures` service on path `/sitestructures`
const createService = require('feathers-mongoose');
const createModel = require('../../models/sitestructures.model');
const hooks = require('./sitestructures.hooks');
const filters = require('./sitestructures.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'sitestructures',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/sitestructures', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('sitestructures');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
