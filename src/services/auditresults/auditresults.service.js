// Initializes the `auditresults` service on path `/auditresults`
const createService = require('feathers-mongoose');
const createModel = require('../../models/auditresults.model');
const hooks = require('./auditresults.hooks');
const filters = require('./auditresults.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'auditresults',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/auditresults', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('auditresults');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
