// Initializes the `webelementcrawleds` service on path `/webelementcrawleds`
const createService = require('feathers-mongoose');
const createModel = require('../../models/webelementcrawleds.model');
const hooks = require('./webelementcrawleds.hooks');
const filters = require('./webelementcrawleds.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'webelementcrawleds',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/webelementcrawleds', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('webelementcrawleds');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
