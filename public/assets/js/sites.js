var isLoading = true;

$(document).ready(function(e){
  $('ul#sidenav02 li').on('click', function(e) {
    $('ul#sidenav02 li .collapse').hide();
    $(this).find('.collapse').show();
  });

  $('ul#sidenav02 li div.collapse ul li').on('click', function(e) {
    // console.log($(this).findElement('ul'));
  });

  $('#contactlink').click = function (){
    $(document).scrollTo('#contact');
  };

  $('div.elems-current-html-item style').remove();

  // var item = $('div.elems-current-html-item');
  $('div.elems-current-html-item').find('*').removeAttr('style');

  $('div.elems-current-html-item').find('a img').each(function(){
    var $elem = $(this);
    $elem.attr('src',$elem.attr('data-src'));
  });
  // $('div.elems-current-html-item').find('*').removeAttr('data-src');

  $('select.form-control').on('change', function(e) {
    window.location.href = '/site/' + $('ul.page-name li a').attr('id') + '/' + $(this).val();
  });

  $('div#page-div').on('click', function (e) {
    window.location.href = '/site/' + $(this).find('ul.page-name li a').attr('id');
  })

  $('span.glyphicon.glyphicon-home').on('click', function (e) {
    window.location.href = '/site/';
  })

  // $('ul#page-name').on('click', function (e) {
  //   window.location.href = '/site/' + $(this).find('li a').attr('id');
  // })

});
