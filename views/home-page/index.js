'use strict'

module.exports = function (app) {
  return function(req,res) {
    const siteService = app.service('sites');
    siteService.find({}).then(sites => {
      if (sites.total > 0) {
        res.render('./home-page/home-page.ejs', {siteInfo: sites.data});
      }
      else {
        res.send('NOT FOUND');
      }
    }).catch(error => {
      console.log(`Error when get Site Id . Error \n: ${error}`);
      res.send('ERROR');
    });
  };
};
