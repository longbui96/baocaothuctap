var moment = require('moment');

module.exports.siteStructure = function (app) {
  return function(req,res) {
    const siteId = req.params.siteId;
    console.log('SITE ID:', siteId);
    const siteService = app.service('sites');
    const captureService = app.service('capturelogs');
    // console.log('siteService :', siteService);
    siteService.find({
      query: {
        siteId: siteId
      }
    }).then(sites => {
      if (sites.total > 0) {
        // res.render('./sites/sites.ejs', {siteInfo: result.data[0]/*, menu: menu*/});
        captureService.find({query: {'$sort': {captureId: -1}}}).then(capture => {
          if (capture.total > 0) {
            res.render('./sites/sites.ejs', {siteInfo: sites.data[0], captInfo: capture.data, moment: moment});
          }
          else {
            res.send('NOT FOUND');
          }
        }).catch(error => {
          console.log(`Error when get Capture Id . Error \n: ${error}`);
          res.send('ERROR');
        });
      }
      else {
        res.send('NOT FOUND');
      }
    }).catch(error => {
      console.log(`Error when get Site Id . Error \n: ${error}`);
      res.send('ERROR');
    });
  };
};

module.exports.siteData = function (app) {
  return function(req,res) {
    const siteId = req.params.siteId;
    const captureId = req.params.captureId;

    console.log('SITE ID:' + siteId);

    const siteService = app.service('sites');
    const crawlElement = app.service('webelementcrawleds');
    const captureLog = app.service('capturelogs');

    // console.log('siteService :', siteService);
    siteService.find({
      query: {
        siteId: siteId
      }
    }).then(site => {
      if (site.total > 0) {
        // res.render('./sites/sites.ejs', {siteInfo: result.data[0]/*, menu: menu*/});
        crawlElement.find({
          query: {
            recordId: captureId,
            weId: {'$regex': siteId},
            '$sort': {weId:1, index:1}
          }
        }).then(elems => {
          if (elems.total > 0) {
            captureLog.find({query: {'$sort': {captureId: -1}}}).then(capture => {
              if (capture.total > 0) {
                //console.log('===================== elems.data: ',elems.data);
                res.render('./sites/sitedata.ejs', { siteInfo: site.data[0], crawlInfo: elems.data, captureInfo: capture.data, moment: moment });
              }
              else {
                res.render('./sites/sitedata.ejs', { siteInfo: site.data[0], crawlInfo: elems.data, captureInfo: null, moment: moment });
              }
            }).catch(error => {
              res.send('================= Error when get Captures Info . Error \n' + error);
            });
          }
          else {
            res.send('NOT FOUND ELEMENTS INFO');
          }
        }).catch(error => {
          res.send('================= Error when get Elements Info . Error \n' + error);
        });
      }
      else {
        res.send('NOT FOUND SITES INFO');
      }
    }).catch(error => {
      res.send('================= Error when get Sites Info . Error \n' + error);
    });
  };
};
