'use strict';

const viewHome = require('./home-page');
const viewSite = require('./sites');
const feathers = require('feathers');

module.exports = function () {
  const app = this;

  // css, font, img, ... store
  app.use('/public', feathers.static(app.get('public')));

  app.use('/site/:siteId/:captureId', viewSite.siteData(app));
  app.use('/site/:siteId', viewSite.siteStructure(app));
  app.use('/', viewHome(app));
};
